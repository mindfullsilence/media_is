/**
 * User: TimAnderson
 * Date: 12/1/15
 * Time: 1:25 PM
 */

/**
 * Test responsive screen sizes by creating a visible-[screen]-block div. Passing multiple screen sizes will return true if one of them is true.
 * See {@link http://codepen.io/mindfullsilence/pen/BjRJvN} for example usage.
 * @requires jQuery
 * @param {(...string|array)} className - The screen(s) to check against. In bootstrap, these are 'xs', 'sm', 'md', 'lg'.
 * @returns {Boolean}
 */

var media_is = (...args) => {
  let sizes = []
  // test if a screen size is currently viewed
  let isVisible = (size) => {
    let result = false
    let doc = document
    let bod = doc.body
    let display = (el) => window.getComputedStyle(el).display
    let testEl = doc
      .createElement('div')
      .classList.add('visible-' + size + '-block')
    bod.appendChild(testEl)
    result = display(testEl) !== 'none'
    bod.removeChild(testEl)
    return result
  }

  // handle required arguments
  if (!args.length) {
    console.warn('The screen size classname is required')
  }

  // convert each argument to an array of strings
  args.forEach((arg, k) => {
    if (!Array.isArray(arg)) {
      args[k] = [String(arg)]
    }
  })

  console.log(args)

  // push all arguments into a flat array
  args.forEach((arg) => {
    arg.forEach(str => {
      sizes.push(str)
    })
  })

  // test for visibility of each size passed. If any pass, return true
  return sizes.some(isVisible)
}

module.exports = media_is
